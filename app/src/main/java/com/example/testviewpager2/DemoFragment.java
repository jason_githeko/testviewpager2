package com.example.testviewpager2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


public class DemoFragment extends Fragment {
    public DemoFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_demo, container, false);

       // Reference Textview in fragment
        TextView textView = view.findViewById(R.id.text_display);
        assert getArguments() != null;

        //Obtain message in bundle
        String message=getArguments().getString("message");

        //Set text in TextView
        textView.setText(message);


        return view;
    }
}