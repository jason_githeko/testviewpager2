package com.example.testviewpager2;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class DemoFragmentCollectionAdapter extends FragmentStateAdapter {

    //Very important - Note format of constructor arguments and super()
    public DemoFragmentCollectionAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }



    @NonNull
    @Override
    public Fragment createFragment(int position) {
        DemoFragment demoFragment = new DemoFragment();
        position++; //Increment variable

        //Create bundle and store message in it
        Bundle bundle = new Bundle();
        bundle.putString("message", "Salamu kutoka Fragimenti  " + position);
        demoFragment.setArguments(bundle);
        return demoFragment;  //Return prepared fragment
    }

    @Override
    public int getItemCount() {
        return 100; //Set number of fragments permitted by pager
    }
}
