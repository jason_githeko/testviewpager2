package com.example.testviewpager2;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;

public class MainActivity extends FragmentActivity { //Note I extend FragmentActivity, not AppComptActivity
    //We are targeting minimum API 19

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Inflate view

        //Reference ViewPager2
        ViewPager2 viewpager = findViewById(R.id.pager);

        //Create adapter .
        // //Very Important  - Note carefully the constructor arguments
        DemoFragmentCollectionAdapter adapter = new DemoFragmentCollectionAdapter(getSupportFragmentManager(),getLifecycle());
        //two arguments expected - FragmentManager and Lifecycle, call these two methods

        viewpager.setAdapter(adapter); //Link adapter to pager
    }

}